#pragma once

#include <stdint.h>

#define DS1803_DEF_ADDR 0

#define VOLTAGE_STEPS 160.0 // max 16V in 100mV steps
#define CURRENT_STEPS 50.0 // max 5A in 100mA steps

typedef enum
{
    POT_VOLTAGE = 0b10101001,
    POT_CURRENT = 0b10101010,
    POT_BOTH = 0b10101111
} 
ds1803_pot_t;

void init_potentiometers(uint8_t addr);
void write_potentiometer (uint8_t addr, ds1803_pot_t pot, uint8_t value);
void on_voltage_pressed_short();
void on_voltage_pressed_long();
void on_voltage_changed(bool up);
void on_current_pressed_short();
void on_current_pressed_long();
void on_current_changed(bool up);
