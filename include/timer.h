#pragma once

#include <util/atomic.h>
#include <avr/interrupt.h>

#define TMR_PREVAL 64                                // prescaler value
#define TMR_PREBITS (_BV(CS01) | _BV(CS00))          // bits for prescaler 64
#define TMR_CMP_MILLIS ((F_CPU / TMR_PREVAL) / 1000) // number of counts per millisceond

#define MILLIS_PIN

void init_timer(void);
uint64_t get_millis(void);
void wait_millis(uint32_t wait_millis);
