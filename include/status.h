#pragma once

#include <stdint.h>
#include <avr/io.h>
#include "timer.h"

#define LED_VOLTAGE PB0                                                                                                                                                                                                                                         
#define LED_CURRENT PB1

#define STATUS_FLASH_DELAY  50

void init_status(void);
void flash_status(uint8_t leds, uint8_t count);
void set_status(uint8_t leds, bool onoff);