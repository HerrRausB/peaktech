#pragma once

#include <avr/interrupt.h>
#include <util/atomic.h>

#define DEBOUNCE_ITVL_MS 5
#define LONG_PRESS_THRESH_MS 500

#define PCINT_PIN_COUNT 6

#define PIN_VOLTAGE_SELECT PA5
#define PIN_VOLTAGE_PIN1 PA0
#define PIN_VOLTAGE_PIN2 PA1

#define PIN_CURRENT_SELECT PA7
#define PIN_CURRENT_PIN1 PA2
#define PIN_CURRENT_PIN2 PA3

typedef void (*pcint_callback_t)(void);


void init_pcint(void);
bool is_set(uint8_t pin);
