#include "timer.h"
#include "status.h"

volatile uint64_t t0_millis = 0;

/*
 * timer 0 interrupt handler
 */

ISR(TIM0_COMPA_vect)
{
    TIMSK0 &= ~_BV(OCIE0A); // disable CTC interrupt
    // ATOMIC_BLOCK(ATOMIC_FORCEON)
    {
        t0_millis++;
    }
    TIMSK0 |= _BV(OCIE0A); // enable CTC interrupt
}

/*
 * API
 */

void init_timer()
{
    // set up timer 0 for CTC mode with interrupt every milliscond

    cli(); // just for good measure

    DDRB |= _BV(PB2);

    TCCR0A = 0; // init registers
    TCCR0B = 0;
    // TCCR0A |= _BV(WGM01); // CTC mode & toggle OC0A / PB2
    TCCR0A |= _BV(WGM01) | _BV(COM0A0); // fast PWM mode & toggle OC0A / PB2
    TCCR0B |= TMR_PREBITS;                                        // set prescaler
    OCR0A = TMR_CMP_MILLIS;                                       // CTC compare value

    TIMSK0 |= _BV(OCIE0A); // enable CTC interrupt

    sei();
}

uint64_t get_millis()
{
    uint64_t m;

    // ATOMIC_BLOCK(ATOMIC_FORCEON)
    {
        m = t0_millis;
    }
    return m;
}

void wait_millis(uint32_t wait_millis)
{
    uint64_t wait_until = get_millis() + wait_millis;

    while (wait_until > get_millis())
        ;
}
