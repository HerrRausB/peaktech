#include "peaktech.h"

volatile uint8_t old_in = 0xFF, new_in = 0;
volatile uint64_t voltage_pressed = 0, current_pressed = 0, last_pcint = 0;

/*
 * pin change interrupt handler
 */

ISR(PCINT0_vect)
{
    uint8_t diff;
    uint64_t now = 0;


    GIMSK &= ~_BV(PCIE0);

    now = get_millis();

    if (now > (last_pcint + DEBOUNCE_ITVL_MS))
    {
        new_in = PINA;

        last_pcint = now;

        diff = new_in ^ old_in;

        if (diff & _BV(PIN_VOLTAGE_SELECT))
        {
            if (!(new_in & _BV(PIN_VOLTAGE_SELECT)))
            //{
            //    // select button pressed
            //    if (!voltage_pressed) // once until button is released again
            //        voltage_pressed = now;
            //}
            //else
            {
                // select button released
                //if (now < (voltage_pressed + LONG_PRESS_THRESH_MS))
                //    on_voltage_pressed_short();
                //else
                    on_voltage_pressed_long();
                voltage_pressed = 0; // reset the short / long indicator
            }
        }

        if ((diff & _BV(PIN_VOLTAGE_PIN1)) &&
            !(new_in & _BV(PIN_VOLTAGE_PIN1)))
        {
            on_voltage_changed(new_in & _BV(PIN_VOLTAGE_PIN2));
        }

        if (diff & _BV(PIN_CURRENT_SELECT))
        {
            if (!(new_in & _BV(PIN_CURRENT_SELECT)))
            {
                // select button pressed
                if (!current_pressed) // once until button is released again
                    current_pressed = now;
            }
            else
            {
                // select button released
                if (now < (current_pressed + LONG_PRESS_THRESH_MS))
                    on_current_pressed_short();
                else
                    on_current_pressed_long();
                current_pressed = 0; // reset the short / long indicator
            }
        }

        if ((diff & _BV(PIN_CURRENT_PIN1)) &&
            !(new_in & _BV(PIN_CURRENT_PIN1)))
        {
            on_current_changed(new_in & _BV(PIN_CURRENT_PIN2));
        }
        old_in = new_in;
    }

    GIMSK |= _BV(PCIE0);
}

/*
 * API
 */

void init_pcint()
{
    cli(); // just for good measures

    // activate pullups on all input pins
    PORTA |= _BV(PIN_VOLTAGE_SELECT) |
             _BV(PIN_VOLTAGE_PIN1) |
             _BV(PIN_VOLTAGE_PIN2) |
             _BV(PIN_CURRENT_SELECT) |
             _BV(PIN_CURRENT_PIN1) |
             _BV(PIN_CURRENT_PIN2);

    // make input pins input pins ;-)
    DDRA &= ~(_BV(PIN_VOLTAGE_SELECT) |
              _BV(PIN_VOLTAGE_PIN1) |
              _BV(PIN_VOLTAGE_PIN2) |
              _BV(PIN_CURRENT_SELECT) |
              _BV(PIN_CURRENT_PIN1) |
              _BV(PIN_CURRENT_PIN2));

    // enable pin change interrupts for selectorpins and one rotary pin each

    PCMSK0 |= _BV(PIN_VOLTAGE_SELECT) |
              _BV(PIN_VOLTAGE_PIN1) |
              _BV(PIN_CURRENT_SELECT) |
              _BV(PIN_CURRENT_PIN1);

    // enable pin change interrupts
    GIMSK |= _BV(PCIE0);

    sei();
}

bool is_set(uint8_t pin)
{
    uint8_t in = PINA;
    return ((in & _BV(pin)) > 0) ? true : false;
}
