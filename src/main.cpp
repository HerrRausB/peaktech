#include "peaktech.h"

#define STATUS_LED PINB1

#define LED_V_SEL PB1
#define LED_V_PIN1 PB2
#define LED_V_PIN2 PA6

uint16_t pos = 0;

// TinyDebugSerial TDS;

void cb_v_sel(uint8_t e)
{
    wait_millis(100);
}

void cb_v_pin1(uint8_t e)
{
    pos += is_set(PIN_VOLTAGE_PIN2) ? 1 : -1;
}

void cb_v_pin2(uint8_t e)
{
    wait_millis(100);
}

void dosome()
{
    uint16_t i, dummy;
    for (i = 0; i < 50000; i++)
        dummy++;
}

void setup()
{
    PORTA = 0;
    PORTB = 0;

    DDRA = 0;
    DDRB = 0;

    init_timer();
    init_status();
    init_potentiometers(DS1803_DEF_ADDR);
    init_pcint();

    set_status(_BV(LED_VOLTAGE),true);
}

uint8_t value = 0,
        step = 1;

void loop()
{
    //write_potentiometer(DS1803_DEF_ADDR, POT_VOLTAGE, value);

    //wait_millis(50);
    //if (value == 255)
    //{
    //    set_status (_BV(LED_VOLTAGE),true);
    //    step = -1;
    //    wait_millis(2000);
    //    set_status (_BV(LED_VOLTAGE),false);
    //}
    //if (value == 0)
    //{
    //    set_status (_BV(LED_CURRENT),true);
    //    step = 1;
    //    wait_millis(2000);
    //    set_status (_BV(LED_CURRENT),false);
    //}
    //value += step;
}
