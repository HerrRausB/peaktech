#include <Wire.h>
#include "peaktech.h"

volatile uint8_t control = 0b00101000;
volatile double val_volt = 0.0,
                val_curr = 0.0,
                step_volt = 15.0,
                step_curr = 15.0;
                //step_volt = (255.0 / VOLTAGE_STEPS),
                //step_curr = (255.0 / CURRENT_STEPS);

volatile bool state_volt = false,
              state_curr = false;

void init_potentiometers(uint8_t addr)
{
    val_volt = 0.0;
    val_curr = 0.0;
    state_volt = 0;
    state_curr = 0;

    Wire.begin();

    write_potentiometer(addr, POT_VOLTAGE, (uint8_t)val_volt);
    write_potentiometer(addr, POT_CURRENT, (uint8_t)val_curr);
}

void write_potentiometer(uint8_t addr, ds1803_pot_t pot, uint8_t value)
{
    Wire.beginTransmission(control | (0b0000111 & addr));
    Wire.write(pot);
    Wire.write(value);
    Wire.endTransmission(true);
}

void on_voltage_pressed_short()
{
    state_volt = state_volt ? false : true;
    set_status(_BV(LED_VOLTAGE),state_volt);
}

void on_voltage_pressed_long()
{
}

void on_voltage_changed(bool up)
{
    if (up)
    {
        if (val_volt < 255.0)
        {
            val_volt += step_volt;
            write_potentiometer(DS1803_DEF_ADDR, POT_VOLTAGE, (uint8_t)val_volt);
        }
    }
    else
    {
        if (val_volt > 0.0)
        {
            val_volt -= step_volt;
            write_potentiometer(DS1803_DEF_ADDR, POT_VOLTAGE, (uint8_t)val_volt);
        }
    }
}

void on_current_pressed_short()
{
    flash_status(LED_CURRENT, 1);
}

void on_current_pressed_long()
{
    flash_status(LED_CURRENT, 2);
}

void on_current_changed(bool up)
{
}
