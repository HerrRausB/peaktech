#include "status.h"

void init_status()
{
    PORTB &= ~(_BV(LED_VOLTAGE) | _BV(LED_CURRENT));
    DDRB |= _BV(LED_VOLTAGE) | _BV(LED_CURRENT);

    flash_status(_BV(LED_VOLTAGE) | _BV(LED_CURRENT), 5);
}

void flash_status(uint8_t leds, uint8_t count)
{
    uint8_t i;

    for (i = 0; i < count; i++)
    {
        if (i)
            wait_millis(STATUS_FLASH_DELAY);
        PORTB |= (leds);
        wait_millis(STATUS_FLASH_DELAY);
        PORTB &= ~(leds);
    }
}

void set_status(uint8_t leds, bool onoff)
{
    if (onoff)
        PORTB |= (leds);
    else
        PORTB &= ~(leds);
}